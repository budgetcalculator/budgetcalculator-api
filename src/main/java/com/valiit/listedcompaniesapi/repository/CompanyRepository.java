package com.valiit.listedcompaniesapi.repository;

import com.valiit.listedcompaniesapi.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;

@Repository
public class CompanyRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public List<Company> getCompanies() {
        return jdbcTemplate.query("select * from company", mapCompanyRows);
    }

    public Company getCompany(int id) {
        List<Company> companies = jdbcTemplate.query("select * from company where id = ?", new Object[]{id}, mapCompanyRows);
        return companies.size() > 0 ? companies.get(0) : null;
    }

    public boolean companyExists(Company company) {
        Integer count = jdbcTemplate.queryForObject("select count(id) from company where name = ?", new Object[]{company.getName()}, Integer.class);
        return count != null && count > 0;
    }

    public void addCompany(Company company) {
        jdbcTemplate.update(
                "insert into company (name, logo, established, employees, revenue, net_income, securities, security_price, dividends) values (?, ?, ?, ?, ?, ?, ?, ?, ?)",
                company.getName(), company.getLogo(), company.getEstablished(), company.getEmployees(), company.getRevenue(),
                company.getNetIncome(), company.getSecurities(), company.getSecurityPrice(), company.getDividends()
        );
    }

    public void updateCompany(Company company) {
        jdbcTemplate.update(
                "update company set name = ?, logo = ?, established = ?, employees = ?, revenue = ?, net_income = ?, securities = ?, security_price = ?, dividends = ? where id = ?",
                company.getName(), company.getLogo(), company.getEstablished(), company.getEmployees(), company.getRevenue(),
                company.getNetIncome(), company.getSecurities(), company.getSecurityPrice(), company.getDividends(), company.getId()
        );
    }

    public void deleteCustomer(int customerNr) {
        jdbcTemplate.update("delete from customers where customerNr = ?", customerNr);
    }

    private RowMapper<Company> mapCompanyRows = (rs, rowNum) -> {
        Company company = new Company();
        company.setId(rs.getInt("id"));
        company.setName(rs.getString("name"));
        company.setLogo(rs.getString("logo"));
        company.setEstablished(rs.getDate("established") != null ? rs.getDate("established").toLocalDate() : null);
        company.setEmployees(rs.getInt("employees"));
        company.setRevenue(rs.getDouble("revenue"));
        company.setNetIncome(rs.getDouble("net_income"));
        company.setSecurities(rs.getInt("securities"));
        company.setSecurityPrice(rs.getDouble("security_price"));
        company.setDividends(rs.getDouble("dividends"));
        return company;
    };

    //calc stuff

    public List<Customer> getCustomers() {
        return jdbcTemplate.query("select * from customers", (row, number) -> {
            return new Customer(
                    row.getInt("customerNr"),
                    row.getString("customerName"),
                    row.getString("contactName"),
                    row.getString("email"),
                    row.getInt("phoneNr"),
                    row.getString("location")
            );
        });
    }

    public Customer getCustomer(String customerName) {
        List<Customer> temp = jdbcTemplate.query("select * from customers where customerName = ?", new Object[]{customerName}, (row, number) -> {
            return new Customer(
                    row.getInt("customerNr"),
                    row.getString("customerName"),
                    row.getString("contactName"),
                    row.getString("email"),
                    row.getInt("phoneNr"),
                    row.getString("location")
            );
        });
        if (temp.size() > 0) {
            return temp.get(0);
        } else {
            return null;
        }
    }

    public void addCustomer(Customer customers) {
        jdbcTemplate.update(
                "insert into customers (customerName, contactName, email, phoneNr, location) values(?, ?, ?, ?, ?)",
                customers.getCustomerName(), customers.getContactName(), customers.getEmail(), customers.getPhoneNr(), customers.getLocation()
        );
    }

    public void updateCustomer(Customer customers) {
        jdbcTemplate.update(
                "update customers set customerName = ?, contactName = ?, email = ?, phoneNr = ?, location = ? where customerNr = ?",
                customers.getCustomerName(), customers.getContactName(), customers.getEmail(), customers.getPhoneNr(), customers.getLocation(), customers.getCustomerNr()
        );
    }

    public void addPriceOffer(PriceOffers priceOffers) {
        jdbcTemplate.update(
                "insert into priceOffers (customerNr, `date`, latestUpdate) values(?, CURRENT_DATE, CURRENT_DATE)",
                priceOffers.getCustomerNr()
        );
    }

    public void updatePriceOffer(PriceOffers priceOffers) {
        jdbcTemplate.update(
                "update priceOffers set customerNr = ?, `date` = ?, latestUpdate = ? where id = ?",
                priceOffers.getCustomerNr(), priceOffers.getDate(), priceOffers.getLatestUpdate(), priceOffers.getId()
        );
    }

    public Customer getCustomerNumber(int customerNr) {
        List<Customer> temp = jdbcTemplate.query("select * from customers where customerNr = ?", new Object[]{customerNr}, (row, number) -> {
            return new Customer(
                    row.getInt("customerNr"),
                    row.getString("customerName"),
                    row.getString("contactName"),
                    row.getString("email"),
                    row.getInt("phoneNr"),
                    row.getString("location")
            );
        });
        if (temp.size() > 0) {
            return temp.get(0);
        } else {
            return null;
        }
    }

    public List<PriceOffers> getPriceoffer() {
        return jdbcTemplate.query("select * from priceoffers", (row, number) -> {
            return new PriceOffers(
                    row.getInt("id"),
                    row.getInt("customerNr"),
                    row.getString("date"),
                    row.getString("latestUpdate")

            );
        });
    }

    public List<PriceOffers> getPriceoffers(int customerNr) {
        return jdbcTemplate.query("select * from priceoffers where customerNr = ?", new Object[]{customerNr}, (row, number) -> {
            return new PriceOffers(
                    row.getInt("id"),
                    row.getInt("customerNr"),
                    row.getString("date"),
                    row.getString("latestUpdate")

            );
        });


    }

    public void addPriceOfferdetails(PriceOfferDetails priceOffersDetails) {
        jdbcTemplate.update(
                "insert into priceofferdetails (siteName, priceOfferId) values(?, ?)",
                priceOffersDetails.getSiteName(), priceOffersDetails.getPriceOfferId()
        );
    }

    // kõigi detailide küsimine
    public List<PriceOfferDetails> getPriceofferDetails() {
        return jdbcTemplate.query("select * from priceofferdetails", (row, number) -> {
            return new PriceOfferDetails(
                    row.getInt("id"),
                    row.getString("siteName"),
                    row.getInt("priceOfferId")
            );
        });
    }

    // Price offer detailidest offer id järgi küsimine
    public List<PriceOfferDetails> getPriceOnePriceOfferDetails(int priceOfferId) {
        return jdbcTemplate.query("select * from priceofferdetails where priceOfferId = ?", new Object[]{priceOfferId}, (row, number) -> {
            return new PriceOfferDetails(
                    row.getInt("id"),
                    row.getString("siteName"),
                    row.getInt("priceOfferId")
            );
        });
    }

    // Price offeri küsimine Id järgi
    public PriceOffers getPriceOfferId(int id) {
        List<PriceOffers> priceOffersList = jdbcTemplate.query("select * from priceoffers where id = ?", new Object[]{id}, (row, number) -> {
            return new PriceOffers(
                    row.getInt("id"),
                    row.getInt("customerNr"),
                    row.getString("date"),
                    row.getString("latestUpdate")

            );
        });
        if (priceOffersList.size() > 0) {
            return priceOffersList.get(0);
        } else {
            return null;
        }
    }

    // Kõigi materjalide küsimine
    public List<Material> getMaterials() {
        return jdbcTemplate.query("select * from materials", (row, number) -> {
            return new Material(
                    row.getInt("id"),
                    row.getString("name"),
                    row.getString("unit"),
                    row.getDouble("purchasePrice")

            );
        });
    }

    // Price offer Dretaili küsimine Id Järgi
    public PriceOfferDetails getDetailsId(int id) {
        List<PriceOfferDetails> temp = jdbcTemplate.query("select * from priceofferdetails where id = ?", new Object[]{id}, (row, number) -> {
            return new PriceOfferDetails(
                    row.getInt("id"),
                    row.getString("siteName"),
                    row.getInt("priceOfferId")
            );
        });
        if (temp.size() > 0) {
            return temp.get(0);
        } else {
            return null;
        }
    }

    // Komponentide küsimine komponendi Id järgi
    public PriceOfferComponent getComponentId(int id) {
        List<PriceOfferComponent> temp = jdbcTemplate.query("select * from priceoffercomponent where id = ?", new Object[]{id}, (row, number) -> {
            return new PriceOfferComponent(
                    row.getInt("id"),
                    row.getInt("priceOfferDetailId"),
                    row.getString("name"),
                    row.getString("description"),
                    row.getDouble("quantity"),
                    row.getString("unit")
            );
        });
        if (temp.size() > 0) {
            return temp.get(0);
        } else {
            return null;
        }
    }

    // Kõikide komponentide küsimine
    public List<PriceOfferComponent> getPOComponent() {
        return jdbcTemplate.query("select * from priceoffercomponent", (row, number) -> {
            return new PriceOfferComponent(
                    row.getInt("id"),
                    row.getInt("priceOfferDetailId"),
                    row.getString("name"),
                    row.getString("description"),
                    row.getDouble("quantity"),
                    row.getString("unit")

            );
        });
    }

    // Komponendist OfferDetail Id Järgi otsimine
    public List<PriceOfferComponent> getPriceOfferDetId(int priceOfferDetailId) {
        List<PriceOfferComponent> components = jdbcTemplate.query("select * from priceoffercomponent where priceOfferDetailId = ?", new Object[]{priceOfferDetailId}, (row, number) -> {
            return new PriceOfferComponent(
                    row.getInt("id"),
                    row.getInt("priceOfferDetailId"),
                    row.getString("name"),
                    row.getString("description"),
                    row.getDouble("quantity"),
                    row.getString("unit")
            );
        });

        for(int i = 0; i < components.size(); i++) {
            components.get(i).setWorks(getWorks(components.get(i).getId()));
        }

        return components;
    }

    // Komponenti lisamine
    public void addPriceOfferComponent(PriceOfferComponent priceOffersComponent) {
        jdbcTemplate.update(
                "insert into priceoffercomponent (priceOfferDetailId, name, description, quantity, unit) values(?, ?, ?, ?, ?)",
                priceOffersComponent.getPriceOfferDetailId(), priceOffersComponent.getName(),
                priceOffersComponent.getDescription(), priceOffersComponent.getQuantity(),
                priceOffersComponent.getUnit()
        );
    }

    // Muuda price offer detaile
    public void updatePriceOfferDetail(PriceOfferDetails priceOfferDetails) {
        jdbcTemplate.update(
                "update priceofferdetails set siteName = ?, priceOfferId = ? where id = ?",
                priceOfferDetails.getSiteName(), priceOfferDetails.getPriceOfferId(), priceOfferDetails.getId()
        );
    }

    // Muuda price offer componente
    public void updatePriceOfferComponent(PriceOfferComponent priceOfferComponent) {
        jdbcTemplate.update(
                "update priceoffercomponent set priceOfferDetailId = ?, name = ?, description = ?, quantity = ?, unit = ? where id = ?",
                priceOfferComponent.getPriceOfferDetailId(), priceOfferComponent.getName(),
                priceOfferComponent.getDescription(), priceOfferComponent.getQuantity(),
                priceOfferComponent.getUnit(), priceOfferComponent.getId()
        );
    }

    // WORK here
    public List<Work> getWorks(int componentId) {
        List<Work> work = jdbcTemplate.query("select * from work where priceOfferComponentId = ?", new Object[] {componentId}, (row, number) -> {
            return new Work(
                    row.getInt("id"),
                    row.getInt("priceOfferComponentId"),
                    row.getInt("materialid"),
                    row.getString("unit"),
                    row.getDouble("quantity"),
                    row.getDouble("quantityPrice"),
                    row.getDouble("installPrice")
            );
        });
        for(int i = 0; i < work.size(); i++) {
            work.get(i).setMaterial(getMaterial(work.get(i).getMaterialId()));
        }

        return work;
    }

    // materjali küsimine Id järgi
    public Material getMaterial(int id) {
        List<Material> materials = jdbcTemplate.query("select * from materials where id = ?", new Object[]{id}, (row, number) -> {
            return new Material(
                    row.getInt("id"),
                    row.getString("name"),
                    row.getString("unit"),
                    row.getDouble("purchasePrice")

            );
        });
        return !materials.isEmpty() ? materials.get(0) : null;
    }



    //Materjali lisamine
    public int addMaterial(Material material) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(connection -> {
                    PreparedStatement ps =
                            connection.prepareStatement(
                                    "insert into materials (name, unit, purchasePrice) values(?, ?, ?)",
                                    Statement.RETURN_GENERATED_KEYS
                            );
                    ps.setString(1, material.getName());
                    ps.setString(2, material.getUnit());
                    ps.setDouble(3, material.getPurchasePrice());
                    return ps;
                },
                keyHolder
        );
        return keyHolder.getKey().intValue();
    }
    // Materjali muutmine
    public void updateMaterial(Material material) {
        jdbcTemplate.update(
                "update materials set  name = ?, unit = ?, purchasePrice = ? where id = ?",
                material.getName(), material.getUnit(), material.getPurchasePrice(), material.getId()
        );
    }
    public void addWork(Work work   ) {
        jdbcTemplate.update(
                "insert into work (priceOfferComponentId, materialId, unit, quantity, quantityPrice, installPrice) values(?, ?, ?, ?, ?, ?)",
                work.getPriceOfferComponentId(), work.getMaterialId(), work.getUnit(), work.getQuantity(), work.getQuantityPrice(), work.getInstallPrice()
        );
    }
    public void updateWork(Work work) {
        jdbcTemplate.update(
                "update work set priceOfferComponentId = ?, materialId = ?, unit = ?, quantity = ?, quantityPrice = ?, installPrice = ? where id = ?",
                work.getPriceOfferComponentId(), work.getMaterialId(), work.getUnit(), work.getQuantity(), work.getQuantityPrice(), work.getInstallPrice(), work.getId()
        );
    }

}

