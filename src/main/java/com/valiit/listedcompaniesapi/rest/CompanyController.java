package com.valiit.listedcompaniesapi.rest;

import com.valiit.listedcompaniesapi.dto.CompanyDto;
import com.valiit.listedcompaniesapi.model.*;
import com.valiit.listedcompaniesapi.repository.CompanyRepository;
import com.valiit.listedcompaniesapi.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/companies")
@CrossOrigin("*")
public class  CompanyController {

    @Autowired
    private CompanyService companyService;

    @Autowired
    private CompanyRepository companyRepository;

    @GetMapping
    public List<CompanyDto> getCompanies() {
        return companyService.getCompanies();
    }

    @GetMapping("/{id}")
    public CompanyDto getCompany(@PathVariable("id") int id) {
        return companyService.getCompany(id);
    }

    @PostMapping
    public void saveCompany(@RequestBody CompanyDto companyDto) {
        companyService.saveCompany(companyDto);
    }

    @DeleteMapping("/{id}")
    public void deleteCompany(@PathVariable("id") int id) {
        companyService.deleteCompany(id);
    }

    @GetMapping("/customers")
    public List<Customer> getAllCustomers() {
        return companyRepository.getCustomers();
    }

    @PostMapping("/customers/search")
    public Object getCustomer(@RequestParam("customerName") String customerName) {
        return companyRepository.getCustomer(customerName);
    }

    @PostMapping("/customers")
    public void addCustomer(@RequestBody Customer customers) {
        companyRepository.addCustomer(customers);
    }

    @PutMapping("/customers")
    public void updateCustomer(@RequestBody Customer customers) {
        companyRepository.updateCustomer(customers);
    }

    @DeleteMapping("/customers/{customerNr}")
    public void deleteCustomers(@PathVariable("customerNr") int customerNr) {
        companyRepository.deleteCustomer(customerNr);
    }

    @GetMapping("/customers/{customerNr}")
    public Customer getCustomerNumber(@PathVariable("customerNr") int customerNr) {
        return companyRepository.getCustomerNumber(customerNr);
    }

    @PostMapping("/priceoffers")
    public void addPriceoffer(@RequestBody PriceOffers priceOffers) {
        companyRepository.addPriceOffer(priceOffers);
    }

    @PutMapping("/priceoffers")
    public void updatePriceoffer(@RequestBody PriceOffers priceOffers) {
        companyRepository.updatePriceOffer(priceOffers);
    }

    @GetMapping("/priceoffers")
    public List<PriceOffers> getAlloffers() {
        return companyRepository.getPriceoffer();
    }

    @PostMapping("/priceOffers/search")
    public Object getPriceoffer(@RequestParam("customerNr") int customerNr) {
        return companyRepository.getPriceoffers(customerNr);
    }

    @PostMapping("/priceofferdetails")
    public void addPriceoffeDetails(@RequestBody PriceOfferDetails priceOfferDetail) {
        companyRepository.addPriceOfferdetails(priceOfferDetail);
    }
    @GetMapping("/priceofferdetails")
    public List<PriceOfferDetails> getAllofferdetails() {
        return companyRepository.getPriceofferDetails();
    }
    @PostMapping("/priceofferdetails/search")
        public Object getPriceOnePriceOfferDetails(@RequestParam("priceOfferId") int priceOfferId) {
            return companyRepository.getPriceOnePriceOfferDetails(priceOfferId);
    }
    @GetMapping("/priceoffers/{id}")
    public PriceOffers getPriceofferId(@PathVariable("id") int id) {
        return companyRepository.getPriceOfferId(id);
    }

    @GetMapping("/materials")
    public List<Material> getAllMaterial(){
        return companyRepository.getMaterials();
    }
    @GetMapping("/priceofferdetails/{id}")
    public PriceOfferDetails getPriceofferDetailsId(@PathVariable("id") int id) {
        return companyRepository.getDetailsId(id);
    }
    @GetMapping("/priceoffercomponent/{id}")
    public PriceOfferComponent getPriceofferComponentId(@PathVariable("id") int id) {
        return companyRepository.getComponentId(id);
    }
    @GetMapping("/priceoffercomponent")
    public List<PriceOfferComponent> getPOComonent() {
        return companyRepository.getPOComponent();
    }
    @PostMapping("/priceoffercomponent/search")
    public List<PriceOfferComponent> getPriceOfferDetId(@RequestParam("priceOfferDetailId") int priceOfferDetailId) {
        return companyRepository.getPriceOfferDetId(priceOfferDetailId);
    }
    @PostMapping("/priceoffercomponent")
    public void addPriceOfferComponent(@RequestBody PriceOfferComponent priceOfferComponent) {
        companyRepository.addPriceOfferComponent(priceOfferComponent);
    }
    @PutMapping("/priceofferdetails")
    public void updatePriceOfferDetail(@RequestBody PriceOfferDetails priceOfferDetails) {
        companyRepository.updatePriceOfferDetail(priceOfferDetails);
    }
    @PutMapping("/priceoffercomponent")
    public void updatePriceOfferComponent(@RequestBody PriceOfferComponent priceOfferComponent) {
        companyRepository.updatePriceOfferComponent(priceOfferComponent);
    }
    @GetMapping("/materials/{id}")
    public Material getMaterialId(@PathVariable("id") int id) {
        return (Material) companyRepository.getMaterial(id);
    }
    @PostMapping("/materials")
    public int addMaterial(@RequestBody Material material) {
        return companyRepository.addMaterial(material);
    }
    @PutMapping("/materials")
    public void updateMaterial(@RequestBody Material material) {
        companyRepository.updateMaterial(material);
    }
    @PostMapping("/work")
    public void addWork(@RequestBody Work work) {
        companyRepository.addWork(work);
    }
    @PutMapping("/work")
    public void updateWork(@RequestBody Work work) {
        companyRepository.updateWork(work);
    }

}