package com.valiit.listedcompaniesapi.model;

public class Material {
    private int id;
    private String name;
    private String unit;
    private double purchasePrice;

    public Material() {
    }

    public Material(int id, String name, String unit, double purchasePrice) {
        this.id = id;
        this.name = name;
        this.unit = unit;
        this.purchasePrice = purchasePrice;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public double getPurchasePrice() {
        return purchasePrice;
    }

    public void setPurchasePrice(double purchasePrice) {
        this.purchasePrice = purchasePrice;
    }
}
