package com.valiit.listedcompaniesapi.model;

public class PriceOffers {
    private int id;
    private int customerNr;
    private String date;
    private String latestUpdate;

    public PriceOffers() {
    }

    public PriceOffers(int id, int customerNr, String date, String lastUpdate) {
        this.id = id;
        this.customerNr = customerNr;
        this.date = date;
        this.latestUpdate = lastUpdate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCustomerNr() {
        return customerNr;
    }

    public void setCustomerNr(int customerNr) {
        this.customerNr = customerNr;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLatestUpdate() {
        return latestUpdate;
    }

    public void setLatestUpdate(String latestUpdate) {
        this.latestUpdate = latestUpdate;
    }
}
