package com.valiit.listedcompaniesapi.model;

import java.util.ArrayList;
import java.util.List;

public class PriceOfferComponent {
    private int id;
    private int priceOfferDetailId;
    private String name;
    private String description;
    private double quantity;
    private String unit;
    private List<Work> works = new ArrayList<>();

    public PriceOfferComponent() {
    }

    public PriceOfferComponent(int id, int priceOfferDetailId, String name, String description, double quantity, String unit) {
        this.id = id;
        this.priceOfferDetailId = priceOfferDetailId;
        this.name = name;
        this.description = description;
        this.quantity = quantity;
        this.unit = unit;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPriceOfferDetailId() {
        return priceOfferDetailId;
    }

    public void setPriceOfferDetailId(int priceOfferDetailId) {
        this.priceOfferDetailId = priceOfferDetailId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public List<Work> getWorks() {
        return works;
    }

    public void setWorks(List<Work> works) {
        this.works = works;
    }
}
