package com.valiit.listedcompaniesapi.model;

public class PriceOfferDetails {
    private int id;
    private String siteName;
    private int priceOfferId;

    public PriceOfferDetails() {
    }

    public PriceOfferDetails(int id, String siteName, int priceOfferId) {
        this.id = id;
        this.siteName = siteName;
        this.priceOfferId = priceOfferId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public int getPriceOfferId() {
        return priceOfferId;
    }

    public void setPriceOfferId(int priceOfferId) {
        this.priceOfferId = priceOfferId;
    }
}
