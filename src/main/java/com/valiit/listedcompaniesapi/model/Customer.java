package com.valiit.listedcompaniesapi.model;

public class Customer {

    private int customerNr;
    private String customerName;
    private String contactName;
    private String email;
    private int phoneNr;
    private String location;

    public Customer() {
    }

    public Customer(int customerNr, String customerName, String contactName, String email, int phoneNr, String location) {
        this.customerNr = customerNr;
        this.customerName = customerName;
        this.contactName = contactName;
        this.email = email;
        this.phoneNr = phoneNr;
        this.location = location;
    }

    public int getCustomerNr() {
        return customerNr;
    }

    public String getCustomerName() {
        return customerName;
    }

    public String getContactName() {
        return contactName;
    }

    public String getEmail() {
        return email;
    }

    public int getPhoneNr() {
        return phoneNr;
    }

    public String getLocation() {
        return location;
    }

    public void setCustomerNr(int customerNr) {
        this.customerNr = customerNr;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhoneNr(int phoneNr) {
        this.phoneNr = phoneNr;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
