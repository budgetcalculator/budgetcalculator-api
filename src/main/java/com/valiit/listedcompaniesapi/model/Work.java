package com.valiit.listedcompaniesapi.model;

import java.util.ArrayList;
import java.util.List;

public class Work {
    private int id;
    private int priceOfferComponentId;
    private int materialId;
    private String unit;
    private double quantity;
    private double quantityPrice;
    private double installPrice;
    private Material material;

    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }

    public Work() {
    }

    public Work(int id, int priceOfferComponentId, int materialId, String unit, double quantity, double quantityPrice, double installPrice) {
        this.id = id;
        this.priceOfferComponentId = priceOfferComponentId;
        this.materialId = materialId;
        this.unit = unit;
        this.quantity = quantity;
        this.quantityPrice = quantityPrice;
        this.installPrice = installPrice;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPriceOfferComponentId() {
        return priceOfferComponentId;
    }

    public void setPriceOfferComponentId(int priceOfferComponentId) {
        this.priceOfferComponentId = priceOfferComponentId;
    }

    public int getMaterialId() {
        return materialId;
    }

    public void setMaterialId(int materialId) {
        this.materialId = materialId;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public double getQuantityPrice() {
        return quantityPrice;
    }

    public void setQuantityPrice(double quantityPrice) {
        this.quantityPrice = quantityPrice;
    }

    public double getInstallPrice() {
        return installPrice;
    }

    public void setInstallPrice(double installPrice) {
        this.installPrice = installPrice;
    }
}
