DROP TABLE IF EXISTS `user`;
DROP TABLE IF EXISTS `company`;
DROP TABLE IF EXISTS `work`;
DROP TABLE IF EXISTS `priceoffercomponent`;
DROP TABLE IF EXISTS `priceofferdetails`;
DROP TABLE IF EXISTS `priceoffers`;
DROP TABLE IF EXISTS `customers`;
DROP TABLE IF EXISTS `materials`;





CREATE TABLE `user` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `username` VARCHAR(190) NOT NULL,
    `password` VARCHAR(190) NOT NULL,
    PRIMARY KEY (id),
    UNIQUE(username)
);

CREATE TABLE `company` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  `logo` VARCHAR(255) NULL,
  `established` DATE NULL,
  `employees` INT NULL,
  `revenue` DECIMAL(12,2) NULL,
  `net_income` DECIMAL(12,2) NULL,
  `securities` INT NULL,
  `security_price` DECIMAL(10,2) NULL,
  `dividends` DECIMAL(10,2) NULL,
  PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS `customers` (
  `customerNr` int(11) NOT NULL AUTO_INCREMENT,
  `customerName` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contactName` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phoneNr` int(15) NOT NULL DEFAULT 0,
  `location` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`customerNr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `materials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `unit` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `purchasePrice` double NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `priceoffers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customerNr` int(11) NOT NULL,
  `date` date NOT NULL,
  `latestUpdate` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_priceoffers_customers` (`customerNr`),
  CONSTRAINT `FK_priceoffers_customers` FOREIGN KEY (`customerNr`) REFERENCES `customers` (`customerNr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `priceofferdetails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `siteName` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `priceOfferId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `priceOfferId` (`priceOfferId`),
  CONSTRAINT `FK_priceofferdetails_priceoffers` FOREIGN KEY (`priceOfferId`) REFERENCES `priceoffers` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `priceoffercomponent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `priceOfferDetailId` int(11) DEFAULT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` double NOT NULL DEFAULT 0,
  `unit` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `priceOfferDetailId` (`priceOfferDetailId`),
  CONSTRAINT `FK_priceoffercomponent_priceofferdetails` FOREIGN KEY (`priceOfferDetailId`) REFERENCES `priceofferdetails` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='tooted';

CREATE TABLE IF NOT EXISTS `work` (
  `priceOfferComponentId` int(11) NOT NULL,
  `materialId` int(11) NOT NULL,
  `unit` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` double NOT NULL DEFAULT 0,
  `quantityPrice` double NOT NULL DEFAULT 0,
  `installPrice` double NOT NULL DEFAULT 0,
  PRIMARY KEY (`priceOfferComponentId`),
  KEY `materialId` (`materialId`),
  CONSTRAINT `FK_work_materials` FOREIGN KEY (`materialId`) REFERENCES `materials` (`id`),
  CONSTRAINT `FK_work_priceoffercomponent` FOREIGN KEY (`priceOfferComponentId`) REFERENCES `priceoffercomponent` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

